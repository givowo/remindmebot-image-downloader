const fs = require('fs')
const http = require('http')
const Stream = require('stream').Transform
const port = 3000
const Reddit = require('reddit')
const readline = require("readline");


const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const reddit = new Reddit({
  username: 'YOURNAME',
  password: 'YOURPASSWORD',
  appId: 'YOURAPPID',
  appSecret: 'YOURAPPSECRET'
});


async function download(type) {
var res;
switch(type){
      case "0":
      res = await reddit.get('/message/unread.json');    
      break;

    case "1":
      res = await reddit.get('/message/messages.json');    
      break;

    default:
      console.log("invalid option!");
      return;
      break;
}
//console.log(res.data);

for (var i = 0; i < res.data.children.length; i++) {
    if (res.data.children[i].data.author == "RemindMeBot") {
      //console.log(res.data.children[i].data);
      var sec;
      var res1;
      if (res.data.children[i].data.context == "") {
         sec = res.data.children[i].data.body.split("/");
         res1 = await reddit.get('/r/' + sec[4] + '/' + sec[5] + '/' + sec[6] + '/' + sec[7] + '.json');
      } else {
         sec = res.data.children[i].data.context.split("/");
         res1 = await reddit.get('/r/' + sec[2] + '/' + sec[3] + '/' + sec[4] + '/' + sec[5] + '.json');
      }
      console.log(res1[0].data.children[0].data.url);

      await http.request('http://' + res1[0].data.children[0].data.url.split("https://")[1], function(response) {
        var data = new Stream();

        response.on('data', function(chunk) {
          data.push(chunk);
        });

        response.on('end', function() {
          fs.writeFileSync(res1[0].data.children[0].data.title + " " + res1[0].data.children[0].data.id + '.png', data.read());
        });
      }).end();

    }
  }

}


rl.question("Which download type? \n 0: unread notifications and messages \n 1: all messages \n", function(type) {
  download(type);
  rl.close();
});



