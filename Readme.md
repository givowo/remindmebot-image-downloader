# How to install/setup/use
So. you want to use it. Well, you're in the right place!  I will try to use images when I can.

## Basic Setup

### Part 1
First you ned the scripts.  The easiest way to is to download the source code as a zip, and then extract `download.js` and `package-lock.json` to the folder where you want the images downloaded to.  Pretty simple. ![srcCode](/srccode.png)

### Part 2
This next part is as complex as this whole thing gets.  right click on `download.js` and select "edit".  this should open the file in notepad, allowing you to see the source code.  there is one part of it that we need to enter info into. Find the part of the file that is like 
```
const reddit = new Reddit({
  username: 'YOURNAME',
  password: 'YOURPASSWORD',
  appId: 'YOURAPPID',
  appSecret: 'YOURAPPSECRET'
});
```
this is what we need to modify.  Enter your reddit username inplace of `YOURNAME` and your reddit password inplace of `YOURPASSWORD`.  make sure to keep the quotes.  for appId and appSecret, you need to mame a reddit app.  go to [here](https://www.reddit.com/prefs/apps) to make a app. scroll down to the button that says "create application".  click it and enter the name of the app, redirect url (can be anything but has to be http:// stylized) and select script as the type. ![create app](/Create.png)
click create and it should now have its own box and information. we'll pull from this. underneath the app name, there is a string of characters. that is the appid. copy that into the quotes to replace `YOURAPPID`.  in the box there should another string of characters labeled "secret". copy that into the quotes to replace `YOURAPPSECRET`. ![info](/Appinfo.png) now we are all done! save and close notepad.

## Installing stuff

### Part 1
with the script setup, we need nodejs and npm to use it.  head to [here](https://nodejs.org/en/) to get the current build. install it **with npm**

### Part 2
with nodejs and npm installed, now its time for the command line. in windows explorer, go to the folder with `download.js`.  it should also have `package-lock.json` in it. in the address bar, where it says the current folder path, type in "cmd" and hit enter. command prompt should open, in this folder! heres a video that i ~~stole~~borrowed ![cmd](/demo.mp4) now type `node --version` and hit enter. you should see and output similar to this: ![nodeout](/nodeout.png) if not, you didnt install nodejs correctly!  now type `npm --version` and hit enter. you should see something like this: ![npmout](/npmout.png) again, if not, you didnt install npm correctly.  please fix the installations to continue. also,the numbers are version numbers, so if they are different from mine thats just fine.

now with node and npm confirmed working, type `npm install` and hit enter. the console will do its thing and when its done, type `npm install reddit` and hit enter. these commands will install the required dependencies for the script! with them done, you can now use the script!

## Usage

now cmd type `node download` and hit enter. if everything has been done correctly, you will see this: ![prompt](/prompt.png) now these different modes download from seperate places. "unread notifications and messages" will download all images from unread remindmebot dms and remindmebot comment reply notifications. "all messages" will only download from messages, both read and unread remindmebot dms.  when youve decided, enter the corresponding number (and nothing else) and hit enter. if you have any messages/notifications from remindmebot in the area that you chose, you will see image urls be put into the console. when. its done, you will see the cursor and be able to enter commands again. look in the folder and see the images!

# troubleshooting
common errors: 

reddit error
-please check your reddit info and the application type. if youve made the wrong type of reddit application, just make a new one and enter the new info.

"module not found"
-make sure to run `npm install` and `npm install reddit`

other errors
-the script has maybe tried to download a link. please use option 0 instead of 1